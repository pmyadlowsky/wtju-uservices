local lapis = require("lapis")
local config = require("lapis.config").get()
local pg = require("lapis.db")
local cjson = require("cjson")
local lfs = require("lfs")
local URL = require("socket.url")
local sha2 = require("sha2")

----- constants
local day_mins = 24 * 60
local day_secs = day_mins * 60
local blind_skip = 30 * 60
local version = "0.5"
local max_vault_age = 14 -- days
local tape_vault = "/mnt/vault"
local tape_vault_url = "https://www.wtju.net/tvault"
local countdown_window_1 = 600
local countdown_window_2 = 60
local countdown_window_3 = 5
local day_names = {"any", "monday", "tuesday", "wednesday",
	"thursday", "friday", "saturday", "sunday"}
local no_playlist = [[
<p>No recent selection information available
for this program.</p>]]
local classical_header = [[
<div><i>&lt;<strong>Composer</strong>&gt:
&lt;Title&gt; {&lt;Performers&gt;} -
&lt;Release notes&gt;</i></div>
]]
local classical_template = [[
<div class="recent-item">
<span class="selection-composer"><strong>%s</strong>:</span>
<span class="selection-title">"%s"</span>
<span class="selection-performer"><i>{%s}</i></span>
%s
</div>
]]
local pop_header = [[
<div><i>&lt;Title&gt; -
&lt;<strong>Performer</strong>&gt;
from &lt;Album&gt; -
&lt;Release notes&gt;</i></div>
]]
local pop_template = [[
<div class="recent-item">
<span class="selection-title">"%s"</span> -
<span class="selection-performer"><strong>%s</strong></span>
<span class="selection-title">from "%s"</span>
%s
</div>
]]
local linked_artist = [[
<a href="https://musicbrainz.org/artist/%s" target="_blank"
title="more at MusicBrainz"><img src="/wp-content/themes/wtju/img/mb-logo.png" alt="MB"/></a>%s
]]
local wtjx_fixed_data = {}
wtjx_fixed_data['station-id'] = "wtjx"
wtjx_fixed_data['live-stream-mp3'] =
	"https://streams.wtju.net:8443/wtjx-128.mp3"
wtjx_fixed_data['live-stream-vorbis'] =
	"https://streams.wtju.net:8443/wtjx-opus-256.ogg"
local wtju_fixed_data = {}
wtju_fixed_data['station-id'] = "wtju"
wtju_fixed_data['live-stream-mp3'] =
	"https://streams.wtju.net:8443/wtju-192.mp3"
wtju_fixed_data['live-stream-vorbis'] =
	"https://streams.wtju.net:8443/wtju-opus-256.ogg"

----- global variables
local tunein_prev_title = ""
local lock_on = false

local function day_name(weekday)
	return day_names[(weekday or 1) + 1]
	end

local function cast_empty_array(array)
	-- For purposes of json encoding, an empty Lua array is
	-- indistinguishable from an empty Lua dictionary. Use
	-- openresty's extension of cjson to force that distinction.
	if #array < 1 then return cjson.empty_array end
	return array
	end

local function send_json(object)
	-- send JSON HTTP response
	ngx.status = ngx.HTTP_OK
	ngx.header.content_type = "application/json; charset=utf-8"
	local json = cjson.encode(object)
	ngx.say(json)
	return ngx.exit(ngx.HTTP_OK)
	end

local function parse_time(str)
	-- accept epoch integer string preceded by "u", or YYYYMMDDHHMM
	-- produce epoch integer
	if str == nil then return nil end
	local m
	m = str:match("^u(%d+)$")
	if m then return tonumber(m) end
	local parts = {}
	local part
	local start, len, year, month, day, hour, min =
		string.find(str, "^(%d%d%d%d)(%d%d)(%d%d)(%d%d)(%d%d)")
	if len then
		return os.time({
			year = tonumber(year),
			month = tonumber(month),
			day = tonumber(day),
			hour = tonumber(hour),
			min = tonumber(min),
			sec = 0
			})
		end
	start, len, year, month, day =
		string.find(str, "^(%d%d%d%d)(%d%d)(%d%d)")
	if len then
		return os.time({
			year = tonumber(year),
			month = tonumber(month),
			day = tonumber(day),
			hour = 0, 
			min = 0,
			sec = 0
			})
		end
	return nil
	end

local function sqlstamp_time(str)
	local start, len, year, month, day, hour, min, sec =
		string.find(str, "(%d%d%d%d)-(%d%d)-(%d%d) (%d%d):(%d%d):(%d%d)")
	if len then
		return os.time({
			year = tonumber(year),
			month = tonumber(month),
			day = tonumber(day),
			hour = tonumber(hour),
			min = tonumber(min),
			sec = tonumber(sec)
			})
		end
	end
local function parse_station(str)
	if str == nil then return "WTJU" end
	str = string.upper(tostring(str))
	if (str == "WXTJ") or (str == "WTJX") then return "WTJX" end
	return "WTJU"
	end

local function wtju_day_of_week(time)
	-- compute day-of-week, with Monday = 1
	local dow = os.date("*t", time).wday - 1
	if dow == 0 then return 7 end -- Sunday
	return dow
	end

local function id_activate(row)
	local tuple = {}
	tuple.id = tonumber(row.id)
	tuple.activate = row.activate
	return tuple
	end

local function sql_time(stamp)
	-- convert SQL timestamp to unix time
	local start, len, year, month, day, hour, min, sec =
		string.find(stamp, "(%d%d%d%d)-(%d%d)-(%d%d) (%d%d):(%d%d):(%d%d)")
	if len == nil then return nil end
	return os.time({
		year = tonumber(year),
		month = tonumber(month),
		day = tonumber(day),
		hour = tonumber(hour),
		min = tonumber(min),
		sec = tonumber(sec)
		})
	end

local function one_off(row)
	-- special one-time show (e.g. marathon)
	if row.retire == nil then return false end
	return ((sql_time(row.retire) - sql_time(row.activate)) < day_secs)
	end

local function started_today(time, row)
	local today = wtju_day_of_week(time)
	local time_rec = os.date("*t", time)
	local now_min = time_rec.hour * 60 + time_rec.min
	local quit = row.start + row.duration
	return (((row.weekday == today) or (row.weekday == 0)) and
		(now_min >= row.start) and (now_min < quit))
	end

local function crossed_midnite(time, row)
	local yesterday = wtju_day_of_week(time - day_secs)
	local time_rec = os.date("*t", time)
	local now_min = time_rec.hour * 60 + time_rec.min
	local ext_min = now_min + day_mins
	local quit = row.start + row.duration
	return ((row.weekday == yesterday) and
		(ext_min >= row.start) and (ext_min < quit))
	end

local function current_show(time, rows)
	-- search through shows to find one current for the given time
	for _, row in pairs(rows) do
		if one_off(row) then return id_activate(row) end
		if started_today(time, row) then return id_activate(row) end
		if crossed_midnite(time, row) then return id_activate(row) end
		end
	return false
	end

local function show_scan(db, stamp, station)
	-- find shows that play today, including those that carry over
	-- from yesterday (cross midnight)
	local sqlstamp = os.date("%Y-%m-%d %H:%M:%S", stamp)
	local today = wtju_day_of_week(stamp)
	local yesterday = wtju_day_of_week(stamp - 24 * 3600)
	local res = db.query([[
		select title, id, activate, weekday, retire, start, duration
			from programs
			where (activate <= ?)
			and ((station = ?) or (station = 'ALL'))
			and ((retire is null) or (retire > ?))
			and ((weekday = ?) or (weekday = ?) or (weekday = 0))
			order by activate desc
		]], sqlstamp, station, sqlstamp, today, yesterday)
	local out = {}
	out.success = (res ~= nil)
	if out.success then out.rows = res end
	return out
	end

local function playing_at(db, time, station)
	local shows_today = show_scan(db, time, station)
	if not shows_today.success then return nil end
	return current_show(time, shows_today.rows)
	end

local function midnite(time)
	-- resolve time to midnight of that day
	local time_rec = os.date("*t", time)
	local offset
	if time_rec.hour < 3 then
		offset = 0
	else
		offset = 3
		end
	return os.time({
		year = time_rec.year,
		month = time_rec.month,
		day = time_rec.day,
		hour = offset,
		min = 0,
		sec = 0
		}) - offset * 3600
	end

local function show_starts(start_min, time)
	local time_rec = os.date("*t", time)
	local now_mins = time_rec.hour * 60 + time_rec.min
	local out = midnite(time) + start_min * 60
	if start_min > now_mins then out = out - day_secs end
	return out
	end

local function show_detail(db, show_id, activate_stamp, time)
	local res = db.query([[
        select title, start, categories.longname as lname,
                programs.duration as dur,
                programs.description as descrip,
                programs.host as host,
                categories.name as cname,
                programs.weekday as weekday,
                programs.subtitle as subtitle,
                programs.activate as activate,
                programs.retire as retire,
                subcats.name as sname,
                subcats.longname as slname,
                programs.archive as archive,
                programs.station as station,
                programs.id as id,
                programs.live_dj as live_dj,
                programs.pre_recorded as pre_recorded,
                programs.off_air as off_air,
                programs.playlist_format as playlist_format
            from programs, categories, progcats, subcats
            where (programs.id = ?)
            and (date_trunc('second',activate) = ?)
            and (progcats.program=programs.id)
            and (subcats.id=progcats.subcat)
            and (categories.id=subcats.maincat)
		]], show_id, activate_stamp)
	local row = res[1]
	return {
		id = row.id, 
		station = row.station,
		title = row.title,
		start = row.start,
		category = row.lname,
		shortcat = row.cname,
		subcat = row.sname,
		host = row.host,
		description = row.descrip,
		duration = row.dur,
		day = row.weekday,
		subtitle = row.subtitle,
		activate = sql_time(row.activate),
		playlist_format = row.playlist_format,
		short_term = one_off(row),
		archive = row.archive,
		starts = show_starts(row.start, time),
		pre_recorded = row.pre_recorded,
		off_air = row.off_air,
		live_dj = row.live_dj
        }
	end

local function show_at_time(db, time, station)
	local show = playing_at(db, time, station)
	if show == nil then return nil end
	return show_detail(db, show.id, show.activate, time)
	end

local function show_data(db, time, station)
	local data = show_at_time(db, time or os.time(), station)
	if data == nil then return nil end
	data.countdown = data.starts + (data.duration * 60) - os.time()
	return data
	end

local function first_show_time(db, station, time)
	local offset = 0
	local show
	while true do
		show = show_at_time(db, time + offset, station)
		if offset >= day_secs then return time end
		if (show and (show.starts >= time)) then
			return show.starts
			end
		offset = offset + 3600
		end
	end

local function collect_shows(db, station, start_date, end_date)
	local bag = {}
	local time_cursor = first_show_time(db, station, midnite(start_date))
	local show, skip
	while true do
		if (time_cursor - midnite(end_date)) >= 0 then
			return bag
			end
		show = show_at_time(db, time_cursor, station)
		skip = ((show == nil) or (show.day == 0))
		if skip then
			time_cursor = time_cursor + blind_skip
		else
			table.insert(bag, show)
			time_cursor = show.starts + show.duration * 60
			end
		end
	end

local function map(list, func)
	local n = #list
	local out = {}
	for i = 1, n do
		out[i] = func(list[i])
		end
	return out
	end

local function clock_time(mins)
	-- convert clock minutes to hhmm
	local hour = math.floor(mins / 60)
	if hour >= 24 then hour = hour - 24 end
	return string.format("%02d%02d", hour, mins % 60)
	end

local function start_time(showrow)
	return clock_time(showrow.start)
	end

local function end_time(showrow)
	return clock_time(showrow.start + showrow.duration)
	end

local function host_detail_less(db, host_id)
	local rows = db.query([[
		select coalesce(air_name, real_name) as name,
				mugshot, biography
			from people
			where (auth_id=?)
		]], host_id)
	if (rows == nil) or (#rows < 1) then return {} end
	local out = {}
	out['host'] = rows[1].name
	out['host-thumb'] = rows[1].mugshot
	out['host-description'] = rows[1].biography
	return out
	end

local function list_hosts(db, show)
	local rows = db.query([[
		select distinct auth_id
			from people, show_hosts
			where (show_hosts.show=?)
			and (show_hosts.host_id=people.auth_id)
		]], show.id)
	local bag = map(rows,
		function(row) return host_detail_less(db, row.auth_id) end)
	if #bag < 1 then return nil else return bag end
	end

local function nominal_host(db, show_id)
	local djs = db.query([[
		select host
			from programs
			where (id=?)
			and ((retire is null) or (retire > current_timestamp))
		]], show_id)
	if (djs == nil) or (#djs < 1) then return nil end
	local out = {}
	out['host'] = djs[1].host
	out['host-thumb'] = nil
	out['host-description'] = nil
	return out
	end

local function primary_djs(db, show)
	local principal = nominal_host(db, show.id)
	if principal then return {principal} end
	local djs = db.query([[
		select auth_id, count(*) as ct
		        from playlist, people, programs
		        where (show=?)
				and (programs.id=?)
				and (programs.retire is null)
				and (created_by=auth_id)
				and people.active
		        group by auth_id
		        order by ct desc
		        limit 4
		]], show.id, show.id)
	return map(djs, function(dj)
		return host_detail_less(db, dj.auth_id)
		end)
	end

local function link_symbol(link_name, counter)
	if link_name == nil then
		return { string.format("url%d", counter + 1), counter + 1 }
	else
		return { link_name, counter }
		end
	end

local function prog_urls(db, show_id)
	local urls = db.query([[
		select display, url
			from prog_urls
			where (program=?)
		]], show_id)
	local url_count = 0
	local bag = map(urls, function(url)
		local sym_info = link_symbol(url.display, url_count)
		url_count = sym_info[2]
		return { sym_info[1], url.url }
		end)
	if #bag < 1 then return nil end
	return bag
	end

local function uniq(list)
	-- remove duplicates from list
	local bag = {}
	for _, item in ipairs(list) do
		bag[item] = true
		end
	local keys = {}
	local i = 1
	for key, value in pairs(bag) do
		keys[i] = key
		i = i + 1
		end
	return keys
	end

local function prog_categories(db, show_id)
	local cats = db.query([[
		select subcats.longname as sname,
				categories.longname as cname
			from subcats, progcats, categories
			where (progcats.program=?)
			and (progcats.subcat=subcats.id)
			and (subcats.maincat=categories.id)
		]], show_id)
	local bag = {}
	for _, cat in ipairs(cats) do
		table.insert(bag, string.lower(cat.sname))
		table.insert(bag, string.lower(cat.cname))
		end
	if #bag < 1 then return nil end
	return uniq(bag)
	end

local function homepage_show_detail(db, show_id)
	local rows = db.query([[
		select title, description, start, duration, id
			from programs
			where (id=?)
			and ((retire is null) or
				(current_timestamp - retire < interval '14 days'))
			order by activate desc
			limit 1
		]], show_id)
	if (rows == nil) or (#rows < 1) then return {} end
	local out = {}
	local row = rows[1]
	out['show-id'] = row.id
	out['show-title'] = row.title
	out['show-description'] = row.description
	out['show-start-time'] = start_time(row)
	out['show-end-time'] = end_time(row)
	out['hosts'] = list_hosts(db, row)
	out['primary-hosts'] = primary_djs(db, row)
	out['show-external-links'] = prog_urls(db, row.id)
	out['show-categories'] = prog_categories(db, row.id)
	return out
	end

local function day_prog(db, station, weekday)
	local ref_date = os.time()
	local start_date = ref_date +
		(weekday - wtju_day_of_week(ref_date)) * day_secs
	local out = {}
	out[day_name(weekday)] =
		map(collect_shows(db, station, start_date, start_date + day_secs),
			function(show)
				return homepage_show_detail(db, show.id)
				end)
	return out
	end

local function marathon_at_time(db, time)
	-- are we in the midst of a fund-drive marathon?
	local now = os.date("%Y-%m-%d %H:%M:%S", time)
	local res = db.query([[
		select genre, start, finish
			from marathon_dates
			where (? >= start)
			and (? < finish)
		]], now, now)
	local out = {}
	out.success = (res ~= nil)
	out.genre = nil
	if out.success and (#res > 0) then
		out.genre = res[1].genre
		out.start = res[1].start
		out.finish = res[1].finish
		end
	return out
	end

local function upcoming_marathon(db)
	-- find next marathon, or one we're in the midst of
	local sqlstamp = os.date("%Y-%m-%d %H:%M:%S", os.time())
	local rows = db.query([[
		select start, finish
			from marathon_dates
			where (start > ?)
			or ((start <= ?) and (finish > ?))
			order by start asc
			limit 1
		]], sqlstamp, sqlstamp, sqlstamp)
	if (rows == nil) or (#rows < 1)  then return nil end
	return {
		start = rows[1].start,
		finish = rows[1].finish
		}
	end

local function marathon_schedule(db, start, finish)
	local rows = db.query([[
		select title, description, start, activate, duration, id
			from programs
			where (activate >= ?)
			and (activate < ?)
			and (retire is not null)
			and (station = 'WTJU')
			order by activate asc, start asc
		]], start, finish)
	if rows == nil then return {} end
	return map(rows,
		function(row)
			local tuple = {}
			tuple['show-timestamp'] =
					os.date("%Y%m%d_%H%M", sqlstamp_time(row.activate))
			tuple['show-id'] = row.id
			tuple['show-title'] = row.title
			tuple['show-description'] = row.description
			tuple['show-start-time'] = start_time(row)
			tuple['show-end-time'] = end_time(row)
			tuple['hosts'] = list_hosts(db, row)
			tuple['primary-hosts'] =
				cast_empty_array(primary_djs(db, row))
			tuple['show-external-links'] = prog_urls(db, row.id)
			tuple['show-categories'] = prog_categories(db, row.id)
			return tuple
			end)
	end

local function host_detail_more(db, host)
	local shows = db.query([[
		select distinct programs.title as title,
				categories.longname as genre
			from show_hosts, programs, progcats,
				subcats, categories
			where (show_hosts.host_id=?)
			and (show_hosts.show=programs.id)
			and (programs.id=progcats.program)
			and (progcats.subcat=subcats.id)
			and (subcats.maincat=categories.id)
			order by title
		]], host.auth_id)
	local out = {}
	out['host'] = host.name
	out['shows'] = map(shows, function(show) return show.title end)
	out['genres'] = uniq(map(shows, function(show) return show.genre end))
	out['host-thumb'] = host.mugshot
	out['host-description'] = host.biography
	return out
	end

local function collect_hosts(db, filter)
	local sfilt = ""
	if filter == "p" then
		sfilt = "and (mugshot is not null)"
		end
	local query = [[
		select auth_id,
				coalesce(air_name, sort_name) as sname,
				coalesce(air_name, real_name) as name,
				biography, mugshot
			from people, auth_groups
			where people.active
			and (auth_id=auth_groups.user_id)
			and (auth_groups.group_id='djs')
		]]
	query = query .. " " .. sfilt .. " order by sname"
	local rows = db.query(query)
	local out = {}
	out['station-id'] = "wtju"
	out['djs'] = cast_empty_array(map(rows,
		function(host) return host_detail_more(db, host) end))
	return out
	end

local function what_codec(user_agent)
	local norm = string.lower(tostring(user_agent))
	if norm:match("iphone;") then return "mp3" end
	if norm:match("chrome/") then return "ogg" end
	if norm:match("firefox/") then return "ogg" end
	if norm:match("konqueror/") then return "ogg" end
	if norm:match("edge/") then return "ogg" end
	return "mp3"
	end

local function pledge_show_menu(db)
	-- populate drop-down menu on pledge form webapp
	local marathon = upcoming_marathon(db)
	if marathon == nil then return {} end
	local station = "WTJU"
	local shows = db.query([[
		select programs.id as id, title, categories.id as cat_id
			from programs, categories, progcats, subcats
			where (activate >= ?)
			and (activate < ?)
			and (retire is not null)
			and (station = ?)
            and (progcats.program=programs.id)
            and (subcats.id=progcats.subcat)
            and (categories.id=subcats.maincat)
			order by activate asc
		]], marathon.start, marathon.finish, station)
	return map(shows,
		function(show)
			return {
				pid = show.id,
				title = show.title,
				cid = show.cat_id
				}
			end)
	end

local function get_psas(db, types, date_order)
	local order
	if date_order == nil then
		order = "long_cat asc, kill asc"
	else
		order = "kill asc, long_cat asc"
		end
	local filter = ""
	if type(types) == "table" then
		filter = " and ("
		local cats = map(types,
			function(t)
				return "(category='" .. t .. "')"
				end)
		filter = filter .. table.concat(cats, " or ")
		filter = filter .. ")"
		end
	local query = [[
		select sponsor, description, content, kill, category,
				login_date,
				psa_cats.long as long_cat
			from psa, psa_cats
			where (kill > current_timestamp)
			and (psa.category=psa_cats.short)
			and psa_cats.publish
			and (login_date <= current_timestamp)
		]]
	query = query .. filter
	query = query .. " order by " .. order
	local psas = db.query(query)
	local date_form = "%B %d, %Y"
	return map(psas,
		function(psa)
			return {
				descrip = psa.description,
				sponsor = psa.sponsor,
				content = psa.content,
				category = psa.long_cat,
				shortcat = psa.category,
				stop = os.date(date_form, sqlstamp_time(psa.kill)),
				start = os.date(date_form, sqlstamp_time(psa.login_date))
				}
			end)
	end

local function publish_stream(db, show_id, start_time)
	local stamp = os.date("%Y-%m-%d %H:%M:%S", start_time)
	local rows = db.query([[
		select stream
			from no_download
			where (show=?)
			and (stamp=?)
		]], show_id, stamp)
	local publish
	if #rows > 0 then
		publish = rows[1].stream
	else
		publish = "t"
		end
	return (publish == "t")
	end

local function playlist(db, show_id, date)
	local rows = db.query([[
		select title, composer, performer, album, released,
				recording_label, notes,
				composer_mbids, performer_mbids,
				album_url, performer_url, composer_url
			from playlist
			where (show=?)
			and (date_played=?)
			order by seq asc
		]], show_id, os.date("%Y-%m-%d", date))
	if (rows == nil) or (#rows < 1) then return nil end
	return map(rows,
		function(row)
			return {
				title = row.title,
				album = row.album,
				album_url = row.album_url,
				performer = row.performer,
				performer_url = row.performer_url,
				composer = row.composer,
				composer_url = row.composer_url,
				year = row.released,
				label = row.recording_label,
				notes = row.notes,
				composer_mbid = row.composer_mbids or "none",
				performer_mbid = row.performer_mbids or "none"
				}
			end)
	end

local function vault_detail(db, show_id, show_start, station, author)
	local rows = db.query([[
		select title, description, weekday, start,
				duration, id, station, host
			from programs
			where (id=?)
			and (station=?)
			order by activate desc
			limit 1
		]], show_id, station)
	if #rows < 1 then return nil end
	local detail = rows[1]
	local show_key = string.format("%d_%d", show_start, show_id)
	local file_base = tape_vault_url .. "/" .. show_key
	local out = {}
	out['show-title'] = detail.title
	out['show-description'] = detail.description
	out['show-date'] = os.date("%Y-%m-%d", show_start)
	out['show-day'] = day_name(detail.weekday)
	out['show-start-time'] = start_time(detail)
	out['show-end-time'] = end_time(detail)
	out['nominal-host'] = detail.host
	out['hosts'] = {host_detail_less(db, author)}
	out['show-external-links'] = prog_urls(db, detail.id)
	out['station-id'] = string.lower(detail.station)
	out['show-categories'] = prog_categories(db, detail.id)
	out['stream-file-mp3'] = file_base .. ".mp3"
	out['stream-file-vorbis'] = file_base .. ".ogg"
	out['show-key'] = show_key
	out['selections'] = playlist(db, detail.id, show_start)
	return out
	end

local function playlist_author(db, show_id, time)
	local rows = db.query([[
		select created_by, count(*) as ct
		        from playlist
		        where (show=?)
				and (date_played=?)
		        group by created_by
		        order by ct desc
		        limit 1
		]], show_id, os.date("%Y-%m-%d", time))
	if (rows == nil) or (#rows < 1) then return "nobody" end
	return rows[1].created_by
	end

local function vet_tape(db, time, show, too_old)
	if time == nil then return nil end
	time = tonumber(time)
	if time <= too_old then return nil end
	show = tonumber(show)
	if not publish_stream(db, show, time) then return nil end
	return {
		show_id = show,
		time = time
		}
	end

local function flac_tapes(db, station, limit)
	local too_old = os.time() - limit * 24 * 3600
	local bag = {}
	local files = {}
	for file in lfs.dir(tape_vault) do table.insert(files, file) end
	table.sort(files, function(a,b) return a < b end)
	for _, file in ipairs(files) do
		local start, len, time, show =
			string.find(file, "(%d+)_(%d+).flac$")
		local tape = vet_tape(db, time, show, too_old)
		if tape ~= nil then
			local detail =
					vault_detail(db, tape.show_id, tape.time, station,
						playlist_author(db, tape.show_id, tape.time))
			if detail ~= nil then
				table.insert(bag, 1, detail)
				end
			end
		end
	return bag
	end

local function get_tunein_creds(db)
	local out = {}
	local rows
	rows = db.query([[
		select value
		from name_value
		where name='tunein_partner_id'
		]])
	if (rows ~= nil) and (#rows > 0) then
		out['partner_id'] = rows[1].value
		end
	rows = db.query([[
		select value
		from name_value
		where name='tunein_partner_key'
		]])
	if (rows ~= nil) and (#rows > 0) then
		out['partner_key'] = rows[1].value
		end
	rows = db.query([[
		select value
		from name_value
		where name='tunein_station_id'
		]])
	if (rows ~= nil) and (#rows > 0) then
		out['station_id'] = rows[1].value
		end
	return out
	end

local function tunein_now_playing(db, playlist_entry, genre)
	local title = tostring(playlist_entry.title)
	if title == tunein_prev_title then return end
	local artist
	if genre == "classical" then
		artist = tostring(playlist_entry.composer)
	else
		artist = tostring(playlist_entry.performer)
		end
	local creds = get_tunein_creds(db)
	local url = "http://air.radiotime.com/Playing.ashx"
	url = url .. "?partnerID=" .. creds.partner_id
	url = url .. "&partnerKey=" .. creds.partner_key
	url = url .. "&id=" .. creds.station_id
	url = url .. "&title=" .. URL.escape(title)
	url = url .. "&artist=" .. URL.escape(artist)
	url = url .. "&album=" .. URL.escape(tostring(playlist_entry.album))
	if os.execute("wget -q -O /dev/null '" .. url .. "'") then
		tunein_prev_title = title
		end
	end

local function recent_selection_row(db, row)
	return {
		stamp = sqlstamp_time(row.air_stamp),
		title = row.title,
		album = row.album,
		album_url = row.album_url,
		year = row.released,
		label = row.recording_label,
		notes = row.notes,
		performer = row.performer,
		performer_url = row.performer_url,
		composer = row.composer,
		composer_url = row.composer_url,
		cquery = row.composer_query,
		pquery = row.performer_query,
		composer_mbid = row.composer_mbids,
		performer_mbid = row.performer_mbids
		}
	end

local function recent_selections(db, limit, show)
	local lim
	if limit == 0 then
		lim = ""
	else
		lim = string.format(" limit %d", limit)
		end
	local query = [[
		select title, composer, performer, album,
				air_stamp, released, recording_label, notes,
				composer_mbids, performer_mbids,
				composer_query, performer_query,
				title_url, performer_url, album_url, composer_url,
				show, date_played, seq
			from playlist
			where (show=?)
			and (air_stamp is not null)
			and ((date_played=current_date) or
				(date_played=(current_date - integer '1')))
			order by date_played desc, seq desc
		]]
	query = query .. lim
	return map(db.query(query, show.id),
			function(row) return recent_selection_row(db, row) end)
	end

local function custom_link(title, url)
	if url and string.match(url, "^https?://") then
		return string.format("<a href=\"%s\" target=\"_blank\">%s</a>",
			url, title)
		end
	return title
	end

local function null_mbid(mbid)
	return ((mbid == nil) or (string.lower(mbid) == "none"))
	end

local function composer_form(item)
	if item.composer then
		local linked_composer =
			custom_link(item.composer, item.composer_url)
		if null_mbid(item.composer_mbid) then
			return linked_composer
		else
			return string.format(linked_artist,
				item.composer_mbid, linked_composer)
			end
		end
	return ""
	end

local function performer_form(item)
	if item.performer then
		local linked_performer =
			custom_link(item.performer, item.performer_url)
		if null_mbid(item.performer_mbid) then
			return linked_performer
		else
			return string.format(linked_artist,
				item.performer_mbid, linked_performer)
			end
		end
	return ""
	end

local function album_form(item)
	if item.album then
		return custom_link(item.album, item.album_url)
		end
	return ""
	end

local function label_year_notes(item)
	local out = ""
	if item.label then
		out = out .. " - " .. item.label
		if item.year then
			out = out .. "[" .. item.year .. "]"
			end
		end
	if item.notes then
		out = out .. " : " .. item.notes
		end
	return out
	end

local function playlist_block(playlist, playlist_format, genre)
	local classical
	if playlist_format == nil then
		classical = (genre == "classical")
	else
		classical = (playlist_format == "classical")
		end
	local out = ""
	if classical then
		out = out .. classical_header
	else
		out = out .. pop_header
		end
	for _, entry in ipairs(playlist) do
		if classical then
			out = out ..
				string.format(classical_template,
					composer_form(entry), entry.title,
					performer_form(entry), label_year_notes(entry))
		else
			out = out ..
				string.format(pop_template,
					entry.title, performer_form(entry),
					album_form(entry), label_year_notes(entry))
			end
		end
	return out
	end

local function virtual_dj(db, station)
	if station ~= "WTJU" then return nil end
	local query = "select value from name_value where (name='virtual_"
	query = query .. string.lower(station) .. "_airboard')"
	local rows = db.query(query)
	if (rows == nil) or (#rows < 1) then return nil end
	local value = rows[1].value
	if value == nil then return nil end
	local start, len, dj_id, year, month, day, hour, min, sec =
		string.find(value,
			"^([^%s]+)%s+(%d+)-(%d+)-(%d+)%s+(%d+):(%d+):(%d+)")
	if len == nil then return nil end
	local stamp = os.time({
		year = tonumber(year),
		month = tonumber(month),
		day = tonumber(day),
		hour = tonumber(hour),
		min = tonumber(min),
		sec = tonumber(sec)
		})
	if stamp < os.time() then return nil end
	return dj_id
	end

local function airboard_dj(db, station)
	local query = "select value from name_value where (name='at_"
	query = query .. string.lower(station) .. "_airboard')"
	local rows = db.query(query)
	if (rows == nil) or (#rows < 1) then return nil end
	return rows[1].value
	end

local function live_host(db, station)
	local dj_id = virtual_dj(db, station) or airboard_dj(db, station)
	if dj_id == nil then return nil end
	local rows = db.query([[
		select coalesce(air_name, real_name) as name,
			mugshot, biography, auth_id
			from people
			where (people.auth_id=?)
		]], dj_id)
	if (rows == nil) or (#rows < 1) then return nil end
	local out = {}
	local row = rows[1]
	out['host'] = row.name
	out['id'] = sha2.sha256hex(row.auth_id)
	out['host-thumb'] = row.mugshot and
		string.format("/media/staff/%s", row.mugshot)
	out['host-description'] = row.biography
	return out
	end

local function countdown_max(show_countdown)
	if lock_on then return countdown_window_3 end
	if show_countdown < countdown_window_2 then
		lock_on = true
		return countdown_window_3
		end
	if show_countdown < countdown_window_1 then
		return countdown_window_2
		end
	return countdown_window_1
	end

local function show_categories(show)
	return uniq({string.lower(show.subcat), string.lower(show.category)})
	end

local function current_show_profile(db, station, show)
	local out = {}
	out['current-show-id'] = show.id
	out['current-show-title'] = show.title
	out['current-show-description'] = show.description
	out['current-show-categories'] = show_categories(show)
	out['current-show-day'] = day_name(show.day)
	out['current-show-start-time'] = start_time(show)
	out['current-show-end-time'] = end_time(show)
	out['current-show-external-links'] = prog_urls(db, show.id)
	out['recent-selections'] = recent_selections(db, 0, show)
	out['current-live-host'] = live_host(db, station)
	local host_rec = {}
	host_rec['host'] = show.host
	host_rec['host-thumb'] = nil
	host_rec['host-description'] = nil
	out['hosts'] = {host_rec}
	return out
	end

local function next_show_profile(db, station, show)
	local clock = show.starts + (show.duration + 1) * 60
	local nxt_show = show_data(db, clock, station)
	if nxt_show == nil then return nil end
	local out = {}
	out['next-show-title'] = nxt_show.title
	out['next-show-description'] = nxt_show.description
	out['next-show-external-links'] = prog_urls(db, nxt_show.id)
	out['next-show-day'] = day_name(nxt_show.day)
	out['next-show-starts'] = nxt_show.starts
	out['next-show-dur'] = nxt_show.duration
	out['next-show-start-time'] = start_time(nxt_show)
	out['next-show-end-time'] = end_time(nxt_show)
	out['next-show-categories'] = show_categories(nxt_show)
	return out
	end

local function show_span(start, dur_mins)
	return (os.date("%I:%M%P", start) .. " - " ..
		os.date("%I:%M%P", start + dur_mins * 60))
	end

local app = lapis.Application()
lock_on = false

app:get("/",
	function(self)
		return send_json({
			environment = config._name,
			port = config.port,
			server = config.server,
			cache = config.code_cache,
			version = version,
			user_agent = ngx.req.get_headers()['user-agent']
			})
		end)
app:get("/*",
	-- catch unrouted requests
	function(self)
		return send_json({splat = self.params.splat})
		end)
app:get("/show-at-time",
	function(self)
		local time = parse_time(self.GET['t']) or os.time()
		local station = parse_station(self.GET['station']) or "WTJU"
		return send_json(show_at_time(pg, time, station))
		end)
app:get("/collect-shows",
	-- over date range for given station
	function(self)
		local start_date = parse_time(self.GET['b']) or os.time()
		local end_date = parse_time(self.GET['e']) or start_date + day_secs
		local station = parse_station(self.GET['station']) or "WTJU"
		return send_json(
			cast_empty_array(
				collect_shows(pg, station, start_date, end_date)))
		end)
app:get("/marathon-at-time",
	function(self)
		local time = parse_time(self.GET['t']) or os.time()
		return send_json(marathon_at_time(pg, time))
		end)
app:get("/hosts",
	function(self)
		local filter = self.GET['f'] or ""
		return send_json(collect_hosts(pg, filter))
		end)
app:get("/marathon-schedule",
	function(self)
		local marathon = upcoming_marathon(pg)
		local sched
		if marathon == nil then
			sched = cjson.empty_array
		else
			sched = cast_empty_array(
				marathon_schedule(pg, marathon.start, marathon.finish))
			end
		return send_json(sched)
		end)
app:get("/stream-podcast",
	-- compose URL for playback of archived recording
	function(self)
		local pid = self.GET['id']
		if pid == nil then return send_json({status = false}) end
		local start, len, ident = string.find(pid, "(%d+_%d+)")
		if ident == nil then return send_json({status = false}) end
		local codec = what_codec(ngx.req.get_headers()['user-agent'])
		return send_json({
			status = true,
			url = "https://www.wtju.net/vault/" .. ident .. "." .. codec
			})
		end)
app:get("/pledge-show-menu",
	function(self)
		return send_json(cast_empty_array(pledge_show_menu(pg)))
		end)
app:get("/psas",
	function(self)
		local order = self.GET['chron']
		return send_json(cast_empty_array(get_psas(pg, nil, order)))
		end)
app:get("/audio-archive",
	function(self)
		local order = self.GET['chron']
		local limit = tonumber(self.GET['limit'] or 1)
		if limit < 1 then
			limit = 1
		elseif limit > max_vault_age then
			limit = max_vault_age
			end
		local station = parse_station(self.GET['station'])
		local out = flac_tapes(pg, station, limit)
		if #out < 1 then
			out = {}
			out['show-title'] = "NO-ARCHIVE"
			end
		return send_json(out)
		end)
app:get("/show-schedule",
	function(self)
		local station = parse_station(self.GET['station'])
		local out = {}
		out['station-id'] = station
		for i = 1, 7 do
			for day, sched in pairs(day_prog(pg, station, i)) do
				out[day] = sched
				end
			end
		return send_json(out)
		end)
app:get("/homepage-playlist",
	function(self)
		local station = parse_station(self.GET['station'])
		local stamp = tonumber(self.GET['stamp'] or 0)
		local limit = tonumber(self.GET['limit'] or 0)
		local show = show_data(pg, nil, station)
		local playlist = recent_selections(pg, limit, show)
		local latest_stamp
		if #playlist < 1 then
			latest_stamp = stamp + 1
		else
			latest_stamp = playlist[1].stamp
			end
		local out
		if latest_stamp <= stamp then
			out = {update = false}
		elseif #playlist < 1 then
			out = {
				update = true,
				stamp = latest_stamp,
				html = no_playlist
				}
		else
			if station == "WTJU" then
				tunein_now_playing(pg, playlist[1], show.shortcat)
				end
			out = {
				update = true,
				stamp = playlist[1].stamp,
				playlist = playlist,
				html = playlist_block(playlist,
					show.playlist_format, show.shortcat)
				}
			end
		return send_json(out)
		end)
app:get("/current-host",
	function(self)
		local station = parse_station(self.GET['station'])
		local prev_id = self.GET['host'] or "none"
		local host = live_host(pg, station)
		local cur_id = ((host and host.id) or "none")
		local out
		if cur_id == prev_id then
			return send_json({update = false})
			end
		local image, hostname
		if host then
			image = host['host-thumb']
		else
			image = nil
			end
		if host then
			hostname = host.host
		else
			local current_show = show_data(pg, nil, station)
			hostname = current_show.host
			end
		out = {
			update = true,
			id = cur_id,
			image = image,
			hostname = hostname
			}
		return send_json(out)
		end)
app:get("/current-show",
	function(self)
		local prev_show = tonumber(self.GET['show'] or 0)
		local station = parse_station(self.GET['station'])
		local show = show_data(pg, nil, station)
		local cur_show = show and show.id
		local countdown = countdown_max(show.countdown)
		local nxt_show = next_show_profile(pg, station, show)
		if prev_show == cur_show then
			return send_json({update = false, countdown = countdown})
			end
		return send_json({
			update = true,
			countdown = countdown,
			id = cur_show,
			title = show.title,
			next_title = nxt_show['next-show-title'],
			day = day_name(show.day):gsub("^%l", string.upper),
			times = show_span(show.starts, show.duration),
			next_times = show_span(nxt_show['next-show-starts'],
				nxt_show['next-show-dur']),
			desc = show.description
			})
		end)
app:get("/current-live",
	function(self)
		local station = parse_station(self.GET['station'])
		local bag
		if station == "WTJX" then
			bag = wtjx_fixed_data
		else
			bag = wtju_fixed_data
			end
		local show = show_data(pg, nil, station)
		if show then
			local src
			src = current_show_profile(pg, station, show)
			for key, value in pairs(src) do bag[key] = value end
			src = next_show_profile(pg, station, show)
			if src then
				for key, value in pairs(src) do bag[key] = value end
				end
			end
		return send_json(bag)
		end)

return app
